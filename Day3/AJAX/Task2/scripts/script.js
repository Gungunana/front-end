$(document).ready(() => {
	$.ajax({
		url: "https://api.openweathermap.org/data/2.5/weather?id=728194&appid=bf685b4c1234e82209ef8c64ff003a9b",
		data: "PlainObject",
		dataType: "json"
	})
	.done( (json) => {
		$("#weather-info").html(
			"Location: lon: " + json.coord.lon + "  lat: " + json.coord.lat + "<br/>" +
			"Temp: " + json.main.temp + "<br/>" +
			"Pressure: " + json.main.pressure + "<br/>" +
			"Humidity: " + json.main.humidity + "<br/>" +
			"Temp min: " + json.main.temp_min + "<br/>" +
			"Temp max: " + json.main.temp_max + "<br/>" +
			"Wind speed: " + json.wind.speed
			);
	})
});