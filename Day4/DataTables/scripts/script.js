$(document).ready( () => {
    $('#users-table').DataTable({
	 ajax: {
        url: 'https://jsonplaceholder.typicode.com/users',
        dataSrc: ''
    },
	 columns: [
            { "data": "name" },
            { "data": "username" },
            { "data": "email" },
        ]
    });
});

// "ajax": {
// 	    "url": "https://jsonplaceholder.typicode.com/users",
// 	    "data": "PlainObject"
// 	    "dataType": "json"
// 	    "type": "GET"
//   		}